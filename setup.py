from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


from setuptools import find_packages
from setuptools import setup

description="Deep learning based tracking reconstruction"

setup(
    name="exatrkx",
    version="1.2.0",
    description="Library for building tracks with Graph Neural Networks.",
    long_description=description,
    author="Exa.TrkX Collaboration",
    author_email="",
    license="Apache License, Version 2.0",
    keywords=["graph networks", "track formation", "tracking", "machine learning"],
    url="https://github.com/exatrkx/exatrkx-iml2020",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        # "tensorflow==2.3.0",
        # "torch==1.7.0",
        # "pytorch-lightning==1.0.6",
        # "torch_cluster==1.5.8",
        # "torch-scatter==2.0.5",
        # "torch-sparse==0.6.8",
        # "torch-geometric==1.6.1",
        # "faiss==1.5.3",
        'trackml@ https://github.com/LAL/trackml-library/tarball/master#egg=trackml-3',
        # "graph_nets>=1.1",
        # "tensorflow",
        # 'horovod',
    ],
    package_data = {
        "exatrkx": ["config/*.yaml"]
    },
    extras_require={
    },
    setup_requires=['trackml'],
    classifiers=[
        "Programming Language :: Python :: 3.7",
    ],
    scripts=[
        'exatrkx/scripts/run_lightning.py',
        'exatrkx/scripts/convert2tf.py',
        'exatrkx/scripts/train_gnn_tf.py',
        'exatrkx/scripts/eval_gnn_tf.py',
        'exatrkx/scripts/tracks_from_gnn.py',
        'exatrkx/scripts/eval_reco_trkx.py',
        'exatrkx/scripts/install_geometric.sh',
        'exatrkx/src/utils_dir.py',
        'exatrkx/src/embedding/embedding_base.py',
        'exatrkx/src/embedding/layerless_embedding.py',
        'exatrkx/src/filter/filter_base.py',
        'exatrkx/src/filter/vanilla_filter.py',
        'exatrkx/src/processing/feature_construction.py',
        'exatrkx/src/processing/utils.py',
        'exatrkx/src/tfgraphs/dataset.py',
        'exatrkx/src/tfgraphs/graph.py',
        'exatrkx/src/tfgraphs/model.py',
        'exatrkx/src/tfgraphs/utils.py',
        'exatrkx/src/torchgnn/GNN_Base.py',
        'exatrkx/src/torchgnn/ResAGNN.py',
        'exatrkx/src/processing/cell_direction_utils/extract_direction.py',
        'exatrkx/src/processing/cell_direction_utils/utils.py',
    ],
)
